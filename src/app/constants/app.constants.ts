export const appC = {
    week: [
        {
            day: 'Mon',
            plateDigits: [1, 2]
        },
        {
            day: 'Tue',
            plateDigits: [3, 4]
        },
        {
            day: 'Wed',
            plateDigits: [5, 6]
        },
        {
            day: 'Thu',
            plateDigits: [7, 8]
        },
        {
            day: 'Fri',
            plateDigits: [9, 0]
        },
        {
            day: 'Sat',
            plateDigits: [-1]
        },
        {
            day: 'Sun',
            plateDigits: [-1]
        }
    ],
    hoursAllowed: {
        morningRange: {
            from: '06:59',
            to: '09:31'
        },
        eveningRange: {
            from: '15:59',
            to: '19:31'
        }
    },
    validation: {
        message: {
            yes: ['Your car can transit at this time', 'alert alert-success'],
            no: ['Your car should not transit at this time', 'alert alert-danger']
        },
        nullInputs: {
            message: ['All fields are required', 'alert alert-warning']
        },
        NaN: {
            message: [`The license plate's last digit is not a number`, 'alert alert-info']
        }
    }

};
