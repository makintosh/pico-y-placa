import { Component } from '@angular/core';
import * as moment from 'moment';
import { appC } from './constants/app.constants';
import { Range } from './models/range.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dateToValidate: Date;
  hourToValidate: string;
  licensePlateNumber: string;

  day: string;
  showValidation = false;
  range: Range;
  validation: any;

  constructor() {
    this.range = new Range();
    this.range.morningFrom = this.stringTimeToMoment(appC.hoursAllowed.morningRange.from);
    this.range.morningTo = this.stringTimeToMoment(appC.hoursAllowed.morningRange.to);
    this.range.eveningFrom = this.stringTimeToMoment(appC.hoursAllowed.eveningRange.from);
    this.range.eveningTo = this.stringTimeToMoment(appC.hoursAllowed.eveningRange.to);
  }

  /**
   * Assign the day of the date selected in a 3 digit format i.e 'Mon' => Monday
   */
  onDateChange(): void {
    // ignore time zone
    this.day = new Date(moment(this.dateToValidate).utc().format()).toDateString().substring(0, 3);
  }

  /**
   * Validate the pico y placa proccess
   */
  validatePicoPlaca(): void {
    if (this.licensePlateNumber && this.hourToValidate && this.dateToValidate) {
      const lastDigitIdx = this.licensePlateNumber.length - 1;
      const plateDigits = appC.week.find(x => x.day === this.day).plateDigits;
      this.validateCurrentDay(plateDigits, lastDigitIdx);
    } else {
      this.validation = appC.validation.nullInputs.message;
    }
    this.showValidation = true;
  }

  /**
   * Validates the selected day
   * @param plateDigits
   * @param lastDigitIdx
   */
  validateCurrentDay(plateDigits: any[], lastDigitIdx: number): void {
    if (!isNaN(+this.licensePlateNumber[lastDigitIdx])) {
      plateDigits.some(x => x === +this.licensePlateNumber[lastDigitIdx]) ?
        this.validateTimeInRange() : this.setsTheMessage(false);
    } else {
      this.validation = appC.validation.NaN.message;
    }
  }


/**
 * Validates the selected time
 */
validateTimeInRange(): void {
  const currentTime = this.stringTimeToMoment(this.hourToValidate);
  if(currentTime.isBetween(this.range.morningFrom, this.range.morningTo) ||
    currentTime.isBetween(this.range.eveningFrom, this.range.eveningTo)) {
  this.setsTheMessage(true);
} else {
  this.setsTheMessage(false);
}
  }

/**
 * Parse the time in a moment format
 * @param time current time i.e '14:00'
 */
stringTimeToMoment(time: string): moment.Moment {
  return moment(time, 'hh:mm');
}

/**
 * Sets the message
 * @param flag
 */
setsTheMessage(flag: boolean): void {
  this.validation = flag ? appC.validation.message.no : appC.validation.message.yes;
}

/**
 * Clean the current search
 */
clean() {
  this.hourToValidate = '';
  this.licensePlateNumber = '';
  this.dateToValidate = null;
  this.showValidation = false;
  document.getElementById('licensePlate').focus();
}

}
